#!/usr/bin/env bash
# alias.sh

shopt -s expand_aliases
alias sudo='sudo -H'

sudo apt-get -y install python3-setuptools
sudo apt-get -y install python-setuptools
sudo apt-get -y install python-pip
sudo apt-get -y install python3-pip

sudo apt-get -y install git
sudo apt-get -y install mercurial

sudo pip install flask
# update: DO NOT USE GHOST!!!! /* render requires Ghost: install Qt4:
#sudo aptitude install qt4-dev-tools http://pyside.readthedocs.org/en/latest/building/linux.html
#sudo apt-get -y install git
#sudo pip install -e git+https://github.com/PySide/pyside-setup.git#egg=PySide

# PySide requires cmake:
#sudo apt-get -y install cmake
#pip install paste
#pip3 install click

# install latest Ghost
#git clone git://github.com/carrerasrodrigo/Ghost.py.git
#cd Ghost.py python setup.py install

# use PyQt for rendering
sudo pip3 install libqt4-dev
# try --user
#  Could not find a version that satisfies the requirement libqt4-dev (from versions: )
# No matching distribution found for libqt4-dev
sudo apt-get -y install python-qt4 qt4-dev-tools python-qt4-dev pyqt4-dev-tools # needs yes
sudo apt-get -y install build-essential python3-dev libqt4-dev # needs yes

sudo wget http://sourceforge.net/projects/pyqt/files/sip/sip-4.16.9/sip-4.16.9.tar.gz
sudo tar -xvf sip-4.16.9.tar.gz
cd sip-4.16.9/
sudo python3 configure.py
sudo make
sudo make install


sudo wget http://sourceforge.net/projects/pyqt/files/PyQt4/PyQt-4.11.4/PyQt-x11-gpl-4.11.4.tar.gz python3 configure.py
sudo make
sudo make install

# --python3 configure.py * --sip-incdir=~/Downloads/sip-4.16.9/siplib* -q /usr/local/Qt-5.1.1/bin/qmake

sudo apt-get -y install tk8.6-dev # needs yes
sudo pip3 install Pillow # change probably to --user
# if getting and error that JPEG not supported, uninstall and reinstall pillow:
# / apt-get -y install libjpeg-dev
# apt-get -y install libfreetype6-dev
# apt-get -y install zlib1g-dev
# apt-get -y install libpng12-dev
# pip3 install PILLOW --upgrade /


sudo pip3 install colormath # needes setuptools module
# pip3 install colormap
# pip3 install easydev
sudo apt-get -y install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose
sudo apt-get -y install sqlite3 libsqlite3-dev

# http://scikit-learn.org/stable/install.html
sudo apt-get -y install build-essential
sudo apt-get -y install python3-dev
sudo apt-get -y install python3-numpy python3-scipy
sudo apt-get -y install libatlas-dev libatlas3gf-base # unable to locate package libatlas3gf-base
# pip3 install --user --install-option="--prefix=" -U scikit-learn
sudo apt-get -y install python-sklearn

# on bronte - X server
sudo apt-get -y install xvfb

# for OAuth need setup toools
sudo pip3 install Flask-SQLAlchemy==1.0
sudo pip3 install Flask-Login
sudo pip3 install rauth
sudo pip install Flask-OAuthlib #--user

# opencv http://www.pyimagesearch.com/2015/07/20/install-opencv-3-0-and-python-3-4-on-ubuntu/
sudo apt-get -y install -f
sudo apt-get -y install cmake pkg-config
sudo apt-get -y install libjpeg8-dev libtiff4-dev libjasper-dev libpng12-dev

# Reading state information... Done
# Package libtiff4-dev is not available, but is referred to by another package.
# This may mean that the package is missing, has been obsoleted, or
# is only available from another source
# However the following packages replace it:
#  libtiff5-dev:i386 libtiff5-dev

sudo apt-get -y install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get -y install libgtk2.0-dev
sudo apt-get -y install gfortran
sudo pip3 install virtualenv virtualenvwrapper # change to --user
#export VIRTUALENVWRAPPER_PYTHON=/usr/local/lib/python3.4
#/usr/local/lib/python3.4
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh
source ~/.bashrc

# make virtual env in mapt-get -y install project folder
mkvirtualenv cv
sudo apt-get -y install python3.5-dev # needs new version
sudo rm -rf ~/.cache/pip/
pip install numpy # already installed

# get opencv
sudo git clone https://github.com/Itseez/opencv.git
cd opencv
sudo git checkout 3.0.0
sudo git clone https://github.com/Itseez/opencv_contrib.git
cd opencv_contrib
sudo git checkout 3.0.0

# different paths on bronte!!! /usr/local/bin

cd ..
sudo mkdir build
cd build
sudo cmake -D CMAKE_BUILD_TYPE=RELEASE \ -D CMAKE_INSTALL_PREFIX=/usr/local \ -D INSTALL_C_EXAMPLES=ON \ -D INSTALL_PYTHON_EXAMPLES=ON \ -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \ -D BUILD_EXAMPLES=ON ..
sudo make -j4
sudo make install
sudo ldconfig
# OpenCV 3.0 should now be installed in /usr/local/lib/python3.4/site-packages/dist-packages/ on my machine
# take note of .so name -- cv2.cpython-34m.so
# In order to use OpenCV 3.0 within our cv virtual environment, we first need
# to sym-link OpenCV into the site-packages directory of the cv environment,
# like this: --from mapt-get -y install project folder

cd ~/.virtualenvs/cv/lib/python3.5/dist-packages/
sudo ln -s /usr/local/lib/python3.5/dist-packages/cv2.cpython-34m.so cv2.so -
# test install
export WORKON_HOME=~/.virtualenvs
sudo workon cv
sudo python3
import cv2 cv2.__version__

# for cleaning up css
# sudo git clone https://github.com/peterbe/mincss
# sudo pip3 install mincss - not using buggy

# for screenshot with phantom.js
sudo apt-get -y install python-pip xvfb xserver-xephyr
sudo pip3 install selenium
sudo pip3 install pyvirtualdisplay
sudo pip3 install pyscreenshot

# for async processing
sudo pip3 install redis
sudo apt-get -y install redis-server
sudo apt-get -y install redis-tools
